<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $username;

    /**
     * @ORM\OneToMany(targetEntity=Approach::class, mappedBy="owner", orphanRemoval=true)
     */
    private $approaches;

    public function __construct()
    {
        $this->approaches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->Username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return Collection|Approach[]
     */
    public function getApproaches(): Collection
    {
        return $this->approaches;
    }

    public function addApproach(Approach $approach): self
    {
        if (!$this->approaches->contains($approach)) {
            $this->approaches[] = $approach;
            $approach->setOwner($this);
        }

        return $this;
    }

    public function removeApproach(Approach $approach): self
    {
        if ($this->approaches->removeElement($approach)) {
            // set the owning side to null (unless already changed)
            if ($approach->getOwner() === $this) {
                $approach->setOwner(null);
            }
        }

        return $this;
    }
}
