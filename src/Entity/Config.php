<?php

namespace App\Entity;

use App\Repository\ConfigRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConfigRepository::class)
 */
class Config
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $linkAccessOnly;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $multipleAnswers;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $anonymous;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $allowComments;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $allowRates;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasDeadline;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deadline;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $registeredUsersOnly;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $allowAdd;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $displayResults;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLinkAccessOnly(): ?bool
    {
        return $this->linkAccessOnly;
    }

    public function setLinkAccessOnly(?bool $linkAccessOnly): self
    {
        $this->linkAccessOnly = $linkAccessOnly;

        return $this;
    }

    public function getMultipleAnswers(): ?bool
    {
        return $this->multipleAnswers;
    }

    public function setMultipleAnswers(?bool $multipleAnswers): self
    {
        $this->multipleAnswers = $multipleAnswers;

        return $this;
    }

    public function getAnonymous(): ?bool
    {
        return $this->anonymous;
    }

    public function setAnonymous(?bool $anonymous): self
    {
        $this->anonymous = $anonymous;

        return $this;
    }

    public function getAllowComments(): ?bool
    {
        return $this->allowComments;
    }

    public function setAllowComments(?bool $allowComments): self
    {
        $this->allowComments = $allowComments;

        return $this;
    }

    public function getAllowRates(): ?bool
    {
        return $this->allowRates;
    }

    public function setAllowRates(?bool $allowRates): self
    {
        $this->allowRates = $allowRates;

        return $this;
    }

    public function getHasDeadline(): ?bool
    {
        return $this->hasDeadline;
    }

    public function setHasDeadline(?bool $hasDeadline): self
    {
        $this->hasDeadline = $hasDeadline;

        return $this;
    }

    public function getDeadline(): ?\DateTimeInterface
    {
        return $this->deadline;
    }

    public function setDeadline(?\DateTimeInterface $deadline): self
    {
        $this->deadline = $deadline;

        return $this;
    }

    public function getRegisteredUsersOnly(): ?bool
    {
        return $this->registeredUsersOnly;
    }

    public function setRegisteredUsersOnly(?bool $registeredUsersOnly): self
    {
        $this->registeredUsersOnly = $registeredUsersOnly;

        return $this;
    }

    public function getAllowAdd(): ?bool
    {
        return $this->allowAdd;
    }

    public function setAllowAdd(?bool $allowAdd): self
    {
        $this->allowAdd = $allowAdd;

        return $this;
    }

    public function getDisplayResults(): ?bool
    {
        return $this->displayResults;
    }

    public function setDisplayResults(?bool $displayResults): self
    {
        $this->displayResults = $displayResults;

        return $this;
    }
}
