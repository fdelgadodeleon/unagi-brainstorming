<?php

namespace App\Entity;

use App\Repository\AlternativeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AlternativeRepository::class)
 * @ORM\Table(name="`alternative`")
 */
class Alternative extends Choice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Vote::class, inversedBy="alternatives")
     * @ORM\JoinColumn(nullable=false)
     */
    private $vote;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVote(): ?Vote
    {
        return $this->vote;
    }

    public function setVote(?Vote $vote): self
    {
        $this->vote = $vote;

        return $this;
    }
}
