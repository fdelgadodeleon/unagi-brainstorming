<?php

namespace App\Repository;

use App\Entity\Brainstorm;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Brainstorm|null find($id, $lockMode = null, $lockVersion = null)
 * @method Brainstorm|null findOneBy(array $criteria, array $orderBy = null)
 * @method Brainstorm[]    findAll()
 * @method Brainstorm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BrainstormRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Brainstorm::class);
    }

    // /**
    //  * @return Brainstorm[] Returns an array of Brainstorm objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Brainstorm
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
